
<!-- 
	Francisco Arellano Herdson Josué
	Ejercicio tarea3_FAHJ
	Viernes 04-agosto-20
-->
<!doctype html>
<html lang="es">
<head>
<meta charset="UTF-8">
<title>Francisco Arellano Herdson Josué</title>
<meta name="Description" content="tarea3 Expresiones Regulares">
</head>
<body>
<center>
<?php

	$curp ='ABCD123456EFGHIJ78'; 
	$correo = "her@gmail.com"; 
	$cadena ="holasasalkdfnskjdfkjsdnfjkbsdfkbsdhfsdfbhfdsbhjdfsbhjfdsbhjdfsbhjdfshbj"; 
	$caracter_especial="&"; 
	$decimal= 7.1; 
	


	$validar_curp= preg_match('/^\D\D\D\D\d\d\d\d\d\d\D\D\D\D\D\D\d\d$/', $curp);
	$validar_correo= preg_match('/^([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5})$/', $correo); 
	$validar_cadena= preg_match('/\w{50,2000}/i', $cadena); 
	$validar_caracter= preg_match('/[^a-zA-Z0-9]/i', $caracter_especial); 
	$validar_decimal= preg_match('/^\d*\.\d+$/', $decimal);

	echo "Curp :", $validar_curp;
	print("\n");
	echo "<br>"; 
	echo  "Correo ", $validar_correo; 
	print("\n"); 
	echo "Validar_cadena: ", $validar_cadena;
	echo "<br>";  
	print("\n"); 
	echo "Validar carácter especial:", $validar_caracter;
	print("\n"); 
	echo "<br>"; 
	echo "Validar decimal:", $validar_decimal;
	print("\n"); 
	echo "<br>";  

?>
<center>
</body>
</html>